﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using NamedPipeServer;

namespace CalculatorService
{
    public partial class CalculatorService : ServiceBase
    {
        private PipeServerManager Server { get; set; }

        public CalculatorService()
        {
            InitializeComponent();
            Server = new PipeServerManager("PipeCalculator");

            if (!EventLog.SourceExists("CalcSource"))
            {
                EventLog.CreateEventSource("CalcSource", "CalcLog");
            }
            eventLog1.Source = "CalcSource";
            eventLog1.Log = "CalcLog";
        }

        protected override void OnStart(string[] args)
        {
            Server.StartServer();
            eventLog1.WriteEntry("Pipe server started it's work.");
        }

        protected override void OnStop()
        {
            Server.StopServer();
            eventLog1.WriteEntry("Pipe server is stopped.");
        }
    }
}
