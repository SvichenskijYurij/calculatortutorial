﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBL.Interfaces;

namespace CalcBL.Classes
{
    public class Action : IAction<double, string>
    {
        private delegate double OperationDelegate(double first, double second);
        private Dictionary<string, OperationDelegate> _operations;

        public Action()
        {
            _operations =
                new Dictionary<string, OperationDelegate>
                {
                    { "+", Add },
                    { "-", Min },
                    { "*", Inc },
                    { "/", Div },
                };
        }

        public double OperationSelect(string key, double arg1, double arg2)
        {
            if (!_operations.ContainsKey(key))
                throw new Exception(String.Format("{0} is unknown operation.", key.ToString()));
            return _operations[key](arg1, arg2);
        }

        public double Add(double first, double second)
        {
            return first + second;
        }

        public double Min(double first, double second)
        {
            return first - second;
        }

        public double Inc(double first, double second)
        {
            return first * second; ;
        }

        public double Div(double first, double second)
        {
            return first / second;
        }
    }
}
