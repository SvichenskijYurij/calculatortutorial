﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBL.Interfaces;

namespace CalcBL.Classes
{
    [Serializable]
    public class Expression: IExpression<double, string>
    {
        public double FirstNumber { get; set; }
        public double SecondNumber { get; set; }
        public double Result { get; set; }
        public string Operation { get; set; }
    }
}
