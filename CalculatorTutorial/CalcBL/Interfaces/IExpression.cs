﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL.Interfaces
{
    public interface IExpression<TValue, TOperation>
    {
        TValue FirstNumber { get; set; }
        TValue SecondNumber{ get; set; }
        TValue Result { get; set; }
        TOperation Operation { get; set; }
    }
}
