﻿namespace CalcBL.Interfaces
{
    /// <summary>
    /// Interface for creating basic calculator logic.
    /// </summary>
    /// <typeparam name="TValue">Tyope of numbers with which you want to perform actions.</typeparam>
    /// <typeparam name="TOperation">Type of operation that you want to perform.</typeparam>
    public interface IAction<TValue, TOperation>
    {
        TValue OperationSelect(TOperation key, TValue arg1, TValue arg2);
        TValue Add(TValue first, TValue second);
        TValue Min(TValue first, TValue second);
        TValue Inc(TValue first, TValue second);
        TValue Div(TValue first, TValue second);
    }
}
