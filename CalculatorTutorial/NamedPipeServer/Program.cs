﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NamedPipeServer
{
    class Program
    {
        private static bool KeppGoing
        {
            get
            {
                var key = Console.ReadKey();
                return !key.Key.Equals(ConsoleKey.Q);
            }
        }

        private static void Main()
        {
            PipeServerManager server = new PipeServerManager("PipeCalculator");
            server.StartServer();
            while (KeppGoing)
            {
                
            }
            server.StopServer();
        }
    }
}
