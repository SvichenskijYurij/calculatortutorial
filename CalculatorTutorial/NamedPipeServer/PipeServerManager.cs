﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBL.Classes;
using CalcBL.Interfaces;
using NamedPipeWrapper;

namespace NamedPipeServer
{
    public class PipeServerManager
    {
        private NamedPipeServer<Expression> Server { get; set; }
        private CalcBL.Classes.Action CalcLogic { get; set; }

        public PipeServerManager(string pipeName)
        {
            Server = new NamedPipeServer<Expression>(pipeName);
            CalcLogic = new CalcBL.Classes.Action();

            Server.ClientMessage += OnClientMessage;
            Server.ClientConnected += OnCliendConnected;
        }

        private void OnCliendConnected(NamedPipeConnection<Expression, Expression> connection)
        {
            connection.PushMessage(new Expression {Operation = String.Empty, FirstNumber = 0, SecondNumber = 0});
        }

        private void OnClientMessage(NamedPipeConnection<Expression, Expression> connection, Expression message)
        {
            message.Result = CalcLogic.OperationSelect(message.Operation, message.FirstNumber, message.SecondNumber);
            connection.PushMessage(message);
        }

        public void StartServer()
        {
            Server.Start();
        }

        public void StopServer()
        {
            Server.Stop();
        }
    }
}
