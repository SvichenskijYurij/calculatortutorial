﻿namespace CalculatorTutorial.Model
{
    public class CalcModel
    {
        public CalcBL.Interfaces.IAction<double, string> Action { get; set; }

        public CalcModel()
        {
            Action = new CalcBL.Classes.Action();
        }
    }
}
