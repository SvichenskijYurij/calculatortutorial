﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBL.Classes;
using NamedPipeWrapper;

namespace CalculatorTutorial.Pipe
{
    public class PipeClientManager
    {
        public  NamedPipeClient<Expression> Client { get; set; }
        public NamedPipeConnection<Expression, Expression> Connection { get; set; }
        public Expression Message { get; set; }

        public PipeClientManager(string pipeName)
        {
            Client = new NamedPipeClient<Expression>(pipeName);
            Message = new Expression();
            //Client.ServerMessage += OnServerMessage;
        }

        //private void OnServerMessage(NamedPipeConnection<Expression, Expression> connection, Expression message)
        //{
        //    Message = message;
        //    _connection = connection;
        //}

        public void SendRequest(string operation, double firstNumber, double secondNumber)
        {
            Message.FirstNumber = firstNumber;
            Message.SecondNumber = secondNumber;
            Message.Operation = operation;

            Connection.PushMessage(Message);
        }

        public void StartClient()
        {
            Client.Start();
        }

        public void StopClient()
        {
            Client.Stop();
        }
    }
}
