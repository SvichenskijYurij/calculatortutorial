﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using CalculatorTutorial.Command;
using CalculatorTutorial.Model;
using CalculatorTutorial.Pipe;
using NamedPipeWrapper;

namespace CalculatorTutorial.ViewModel
{
    public class CalcViewModel : CalcViewModelBase
    {
        private CalcModel _calcModel;
        private PipeClientManager _client { get; set; }

        public ICommand digitButtonPress { get; set; }
        public ICommand actionButtonPress { get; set; }
        public ICommand additionalButtonPress { get; set; }

        private string _memory;
        public string Memory
        {
            get
            {
                return _memory;
            }
            set
            {
                _memory = value;
                OnPropertyChanged("Memory");
            }
        }

        private string _action;
        public string Action
        {
            get
            {
                return _action;
            }
            set
            {
                _action = value;
                OnPropertyChanged("Action");
            }
        }

        private string _inputNumber;
        public string InputNumber
        {
            get
            {
                return _inputNumber;
            }
            set
            {
                _inputNumber = value;
                OnPropertyChanged("InputNumber");
            }
        }

        public CalcViewModel()
        {
            _client = new PipeClientManager("PipeCalculator");
            _calcModel = new CalcModel();
            digitButtonPress = new RelayCommand(DigitButtonPress);
            actionButtonPress = new RelayCommand(ActionButtonPress);
            additionalButtonPress = new RelayCommand(AdditionalButtonPress);
            InputNumber = string.Empty;
            Action = string.Empty;
            Memory = "0";

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-IN");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-IN");
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(
                        XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            _client.Client.ServerMessage += OnServerMessage;
            _client.StartClient();
        }

        private void OnServerMessage(NamedPipeConnection<CalcBL.Classes.Expression, CalcBL.Classes.Expression> connection, CalcBL.Classes.Expression message)
        {
            _client.Connection = connection;
            Memory = message.Result.ToString();
        }

        public void DigitButtonPress(object param)
        {
            if (param.ToString().Equals(".") && InputNumber.Equals(string.Empty)) { }
            else if (param.ToString().Equals(".") && !InputNumber.Contains("."))
            {
                InputNumber += param.ToString();
            }
            else if (!param.ToString().Equals("."))
            {
                InputNumber += param.ToString();
            }
        }

        public void AdditionalButtonPress(object param)
        {
            if (param.ToString().Equals("C"))
            {
                Memory = "0";
                InputNumber = string.Empty;
                Action = string.Empty;
            }
            if (param.ToString().Equals("Del"))
            {
                if (!InputNumber.Equals(string.Empty))
                    InputNumber = InputNumber.Substring(0, InputNumber.Length - 1);
            }
        }

        public void ActionButtonPress(object param)
        {
            if (param.ToString().Equals("=") && !InputNumber.Equals(string.Empty) && !Action.Equals(string.Empty))
            {
                Memory = _calcModel.Action.OperationSelect(Action, Convert.ToDouble(Memory), Convert.ToDouble(InputNumber)).ToString();
                //_client.SendRequest(Action, Convert.ToDouble(Memory), Convert.ToDouble(InputNumber));
                InputNumber = string.Empty;
                Action = string.Empty;
            }
            else if(param.ToString().Equals("=") && InputNumber.Equals(string.Empty)) { }
            else if (param.ToString().Equals("=") && string.IsNullOrEmpty(Action) && !InputNumber.Equals(string.Empty))
            {
                Memory = InputNumber;
                InputNumber = string.Empty;
            }
            else if (!Action.Equals(param.ToString()) && !Action.Equals(string.Empty) && !InputNumber.Equals(string.Empty))
            {
                Memory = _calcModel.Action.OperationSelect(Action, Convert.ToDouble(Memory), Convert.ToDouble(InputNumber)).ToString();
                //_client.SendRequest(Action, Convert.ToDouble(Memory), Convert.ToDouble(InputNumber));
                Action = param.ToString();
                InputNumber = string.Empty;
            }
            else if (Action.Equals(param.ToString()) && !Action.Equals(string.Empty) && !InputNumber.Equals(string.Empty))
            {
                Memory = _calcModel.Action.OperationSelect(Action, Convert.ToDouble(Memory), Convert.ToDouble(InputNumber)).ToString();
                //_client.SendRequest(Action, Convert.ToDouble(Memory), Convert.ToDouble(InputNumber));
                Action = param.ToString();
                InputNumber = string.Empty;
            }
            else if (Action.Equals(string.Empty) && !InputNumber.Equals(string.Empty))
            {
                Action = param.ToString();
                Memory = InputNumber;
                InputNumber = string.Empty;
            }
            else if (InputNumber.Equals(string.Empty))
                Action = param.ToString();
        }
    }
}
